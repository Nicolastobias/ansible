#!/bin/sh

for s in $(docker service ls | grep -v NAME | awk '{print $2}')
do 
	# https://github.com/docker/docker/issues/30981#issuecomment-279528190
	docker service update --force $s
	sleep 10
done

