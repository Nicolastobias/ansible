#!/bin/bash

start-swarm-service.sh wallet-async  $*
sleep 20
start-swarm-service.sh wallet-sync  $*
sleep 20
start-swarm-service.sh wallet-query $*
sleep 20
start-swarm-service.sh payments-srv $*
sleep 20
start-swarm-service.sh middleware-jobs $*
sleep 20
start-swarm-service.sh middleware-webservices $*
sleep 20
start-swarm-service.sh casino-srv $*
sleep 20
start-swarm-service.sh reports-srv $*
sleep 20
start-swarm-service.sh admintool $*
sleep 20
start-swarm-service.sh knockoutcasino $*
sleep 20
start-swarm-service.sh game-lobby $*
sleep 20
start-swarm-service.sh kafka-connect-couchbase-standalone $*
sleep 20
start-swarm-service.sh transaction-data-connector $*
sleep 20
start-swarm-service.sh website $*
sleep 20
start-swarm-service.sh platform-srv $*

sleep 20
start-swarm-service.sh bet-history-service $*



