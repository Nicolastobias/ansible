#!/bin/sh

SVC=$1
TAG=$2
if [ "$SVC" = "" -o "$TAG" = "" ]
then
        echo "Usage: $0 <svc name> <tag>"
        exit 1
fi

WHICH=middleware
if [ "$SVC" = "admintool" -o "$SVC" = "knockoutcasino" -o "$SVC" = "website" ]
then
    WHICH=node
fi 

docker -vv service update --image nexus-corp.knockout.local:18444/knockout-${WHICH}-${SVC}:${TAG} ${SVC}

