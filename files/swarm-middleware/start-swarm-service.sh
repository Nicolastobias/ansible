#!/bin/sh
CMD=
SVC=$1
PORT=$2
if [ "$SVC" = "" ]
then
	echo "Usage: $0 <svc name>"
	exit 1
fi

NUM_REPLICAS=2 # initial number of replicas per service
UPDATE_DELAY=5s # time between rolling updates for docker swarm service
BIND_DIR=$SVC # by default the BIND_DIR for /config matches /opt/ko/$SVC 
FAR_PORT=8080 # default for middleware, override for node
FAR_JMX_PORT=18081 # default for middleware only
LOG_DIR=/logs # default for middleware, override for node


case $SVC in
wallet-async)
    PORT=8081
    JMX_PORT=18081
    WHICH=middleware
    ;;
wallet-sync)
    PORT=8082
    JMX_PORT=18082
    WHICH=middleware
    ;;
wallet-query)
    PORT=8083
    JMX_PORT=18083
    WHICH=middleware
    ;;
payments-srv)
    BIND_DIR=payments-srv
    PORT=8084
    JMX_PORT=18084
    WHICH=middleware
    ;;
middleware-jobs)
    BIND_DIR=midware-jobs
    PORT=8085
    JMX_PORT=18085
    WHICH=middleware
    ;;
middleware-webservices)
    PORT=8086
    JMX_PORT=18086
    BIND_DIR=middleware-ws
    WHICH=middleware
    ;;
casino-srv)
    BIND_DIR=casino-srv
    PORT=8087
    JMX_PORT=18087
    WHICH=middleware
    ;;
reports-srv)
    BIND_DIR=reports-srv
    PORT=8088
    JMX_PORT=18088
    WHICH=middleware
    ;;
admintool)
    BIND_DIR=""
    PORT=8089
    WHICH=node
    FAR_PORT=3000
    LOG_DIR=app/admintool/logs

    ;;
knockoutcasino)
    BIND_DIR=""
    PORT=8090 
    WHICH=node
    FAR_PORT=3000
    ;;
game-lobby)
    PORT=8091
    JMX_PORT=18091
    WHICH=middleware
    ;;
kafka-connect-couchbase-standalone)
    PORT=8092
    JMX_PORT=18092
    WHICH=middleware
    ;;

transaction-data-connector)
    PORT=8093
    JMX_PORT=18093
    WHICH=middleware
    ;;

website)
    PORT=8094
    JMX_PORT=18094
    WHICH=node
    LOG_DIR=app/logs
    ;;
platform-srv)
    PORT=8095
    JMX_PORT=18095
    WHICH=middleware
    ;;

bet-history-service)
    PORT=8096
    JMX_PORT=18096
    WHICH=middleware
    ;;




portainer)
    CMD="docker service create 
        --name portainer 
        --publish 9000:9000 
        --constraint 'node.role == manager' 
        --with-registry-auth 
        --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock 
        portainer/portainer --no-auth"
    ;;
esac

echo $*


if [ "$2" = "-pull" ]
then
  docker pull nexus-corp.knockout.local:18444/knockout-${WHICH}-${SVC}:latest
  exit $?
fi

if [ "$2" = "-update" ]
then
  docker -vv service update --image nexus-corp.knockout.local:18444/knockout-${WHICH}-${SVC}:latest ${SVC}
  exit $?
fi

NET_NAME=knockout-net

# First look to see if we have a '${NET_NAME}' network, and if not create it
# then make the services use it

docker network list | grep -v NAME | awk '{print $2}' | grep ${NET_NAME} 2>&1 >& /dev/null
if [ $? != 0 ]
then
    # network does not exist, create it ( see also https://github.com/docker/docker/issues/25796#issuecomment-240384692)
    docker network create --subnet 10.100.0.0/16 --driver overlay --opt secure ${NET_NAME}

    #docker network create --subnet 10.25.140.0/23 --gateway 10.24.140.66 -o parent=ens192 --driver macvlan --opt secure ${NET_NAME}
fi

mkdir -p /opt/ko/${SVC}



CMDLINE="--with-registry-auth --name ${SVC} --publish ${PORT}:${FAR_PORT}"

if [ "$BIND_DIR" != "" ]
then
    CMDLINE="$CMDLINE --mount type=bind,source=/opt/ko/${BIND_DIR}/,destination=/config --publish ${JMX_PORT}:${FAR_JMX_PORT} "
fi    
CMDLINE="$CMDLINE --mount type=volume,source=${SVC}-logs,destination=${LOG_DIR} \
   --update-delay ${UPDATE_DELAY} --limit-memory 3gb --replicas ${NUM_REPLICAS} --network ${NET_NAME} \
   nexus-corp.knockout.local:18444/knockout-${WHICH}-${SVC}:latest"


echo "CMD=$CMD"
if [ "$CMD" != "" ]
then
    echo $CMD
    "$CMD"
else
    echo docker service create ${CMDLINE}
    docker service create ${CMDLINE}
fi
