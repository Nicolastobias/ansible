#!/bin/sh

if [ "$1" = "" ]
then
	echo "Usage: $0 <final scale>"
        exit 1
fi

SCALE=$1

for s in $(docker service ls | grep -v NAME | awk '{print $2}')
do 
(
	echo $s 
	docker service scale ${s}=$(( $SCALE * 2 ))
        echo sleeping 60 for $s
	sleep 60 	
	docker service scale ${s}=$SCALE
	sleep 2  
) &
done

