#!/bin/bash

set -x

bucket_name=$1
bucket_port=$2
bucket_ram_quota=$3
num_replicas=$4
admin_user=$5
admin_password=$6
kenv=$7

/opt/couchbase/bin/couchbase-cli bucket-create -c couchbase-1.$kenv.knockout.local:8091 --bucket=$bucket_name \
--bucket-type=couchbase --bucket-port=$bucket_port --bucket-ramsize=$bucket_ram_quota \
--bucket-replica=$num_replicas --u=$admin_user --p=$admin_password

