---
date: 29th August 2018
who: Nicolas Tobias
---


Hi.

This is the new version of the */build-configuration/ansible-automation* config folder.
Here you will found all the ansible scripts categorized in differents ways.

## Playbooks

There are, for the moment, two different kinds of Playbooks
* ADHOC-[name]
* INSTALL-[name]

### ADHOC-[name]
This playbooks does specific tasks, normally it execute specific roles or execute ansible tasks
i.e
```
---
#NOTE This playbook reduces the replicas to 0 and then waits for the port to be ready and
#increase to 2
- hosts: docker-servers
  remote_user: ko
  become: yes
  run_once: true

  tasks:
  - name: "ADHOC | Reduce configserver replicas to 0 and wait 1 minute..."
    shell: docker service update --replicas=0 config-service_config-server
  - pause:
      seconds: 10
  - name: "ADHOC | Increasing replicas on docker..."
    shell: docker service update --replicas=2 config-service_config-server

  - name: "ADHOC | Wait for the port: {{ csrv_port }} to be accesible again..."
    uri:
      url: "http://{{ csrv_hostname }}:{{ csrv_port }}/{{ csrv_path }}"
      status_code: 200
      validate_certs: False
      headers:
        x-config-token: "{{ csrv_token }}"
    register: result
    until: result | succeeded
    retries: 5
    delay: 5
    tags: waitfor
```

Or it can execute specific roles instead (like adding/removing ssh users) i.e
```
---
- hosts: all
  remote_user: ko
  become: yes

  roles:
    - role: ssh-key-users
```


### INSTALL-[name]
This are playbook which executed a series of more than one roles This should be the most used ones, so in case
we want to deploy a now mariadb server node, we have to run the playbok `INSTALL-mariadb` and it will execute several
roles until the endpoint adquire the desired state. i.e:
```
- hosts: mysql-servers,mariadb-servers,bbdd-bi
  remote_user: ko
  become: yes

  roles:
    - { role: baseline, tags: "baseline" }
    - { role: ssh-key-users, tags: "users"}
    - { role: mariadb-server-galera, tags: "dbserver" }
    - { role: bareos-common, tags: "bareos" }
    - { role: bareos-client, tags: "bareos" }
    - { role: database-monitor, tags: "pmm-client"}
```

It can also contains tags, that can be used as a shortcut to different parts of the process, but, if you are going to
run a tag more than once, you have to consider create an ADHOC one for this specific task (i.e adding mysql users to
database on the deployment of the server, but then we need to manage them, then goto ADHOC playbook )

## Roles
These are the fundamental bricks where the playbooks are made of. This executes different parts of code to reach desired
states. A set of them in a specific order, is what generates a playbook.
For the moment, we have two different categories:

* Service-[name]
* Config-[name]

### Service-[name]
This are complete roles that deploys an specific service like haproxy, mariadb, etc.
This should be executed once on a target host (like deploy mariadb on a server )

### Config-[name]
This are playbooks that configures specific things, like ssh users, network rules, folders, etc.
This should be executed several times on a targer host (like add or remove users to a mysql db)
