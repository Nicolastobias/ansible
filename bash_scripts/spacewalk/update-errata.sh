#!/bin/bash

cd /opt/scripts

wget -N http://cefs.steve-meier.de/errata.latest.xml

export SPACEWALK_USER\=***keeper spacewalk user website***
export SPACEWALK_PASS\=***keeper spacewalk password website***

perl errata-import.pl --server spacewalk-corp.knockout.local --errata errata.latest.xml --publish > /dev/null 2>&1
