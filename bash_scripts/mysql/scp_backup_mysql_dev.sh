#!/bin/bash
# Remote scp credentials

 user="root"
 password=""
 host="10.35.40.4"
 file_name="KOSchema-DEV-"

# Other options

 path="/opt/backup/mysql"
 backup_log="/opt/scripts/logs"
 date=$(date +"%d-%b-%Y")

# Set default file permissions

 umask 177

echo `date -u` "Starting downloading file" $file_name$date".gz to" $path >>  $backup_log/$file_name$date.log

sshpass -p "$password" scp $user@$host:$path/$file_name$date.sql.gz $path/$file_name$date.sql.gz

echo `date -u` "Finished downloading file" $file_name$date".gz to" $path >>  $backup_log/$file_name$date.log

echo `date -u` "Deleting files older than 10 days on " $path "and " $backup_log >> $backup_log/$file_name$date.log 

 find $path/* -mtime +10 -exec rm {} \;
 find $backup_log/* -mtime +10 -exec rm {} \;

echo `date -u` "Finished Delete files older than 10 days on " $path "and " $backup_log >> $backup_log/$file_name$date.log
