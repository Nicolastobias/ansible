#!/bin/bash
# Database credentials

 user="ko"
 password=""
 host="localhost"
 db_name="KOSchema"
 env="PROD"

# Other options

 backup_path="/opt/backup/mysql"
 backup_log="/opt/scripts/logs"
 date=$(date +"%d-%b-%Y")

# Set default file permissions

 umask 177

echo `date -u` "Starting Dump database into SQL file mysql_backup-"$env"-"$date".sql" >>  $backup_log/mysql_backup-$env-$date.log

  mysqldump -u$user -p$password $db_name > $backup_path/$db_name-$env-$date.sql

echo `date -u` "Finished Dump database into SQL file mysql_backup-"$env"-"$date".sql" >>  $backup_log/mysql_backup-$env-$date.log

echo `date -u` "Starting compress file mysql_backup-"$env"-"$date".sql to mysql_backup-"$env"-"$date".sql.gz" >>  $backup_log/mysql_backup-$env-$date.log

 gzip $backup_path/$db_name-$env-$date.sql
 chown drewdurkin:root $backup_path/$db_name-$env-$date.sql.gz

echo `date -u` "Finished compress file mysql_backup-"$env"-"$date".sql to mysql_backup-"$env"-"$date".sql.gz" >>  $backup_log/mysql_backup-$env-$date.log


echo `date -u` "Deleting files older than 10 days on " $backup_path "and " $backup_log >> $backup_log/mysql_backup-$env-$date.log 

 find $backup_path/* -mtime +10 -exec rm {} \;
 find $backup_log/* -mtime +10 -exec rm {} \;

echo `date -u` "Finished Delete files older than 10 days on " $backup_path "and " $backup_log >> $backup_log/mysql_backup-$env-$date.log
