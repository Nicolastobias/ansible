#!/bin/bash
# Remote scp credentials
 
 user="drewdurkin"
 password=""
 host="81.88.163.164"
 file_name="KOSchema-PROD-"
 port="1152"

# Other options

 path="/opt/backup/mysql"
 backup_log="/opt/scripts/logs"
 date=$(date +"%d-%b-%Y")

# Set default file permissions

 umask 177

echo `date -u` "Starting downloading file" $file_name$date".sql.gz to" $path >>  $backup_log/$file_name$date.log

sshpass -p "$password" scp -P $port -r $user@$host:$path/$file_name$date.sql.gz $path/$file_name$date.sql.gz

echo `date -u` "Finished downloading file" $file_name$date".sql.gz to" $path >>  $backup_log/$file_name$date.log

echo `date -u` "Deleting files older than 10 days on " $path "and " $backup_log >> $backup_log/$file_name$date.log 

 find $path/* -mtime +30 -exec rm {} \;
 find $backup_log/* -mtime +30 -exec rm {} \;

echo `date -u` "Finished Delete files older than 30 days on " $path "and " $backup_log >> $backup_log/$file_name$date.log
