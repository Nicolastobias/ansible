#!/bin/bash

envloc="../env/"

echo "Enter your old user:"
read ouser

echo "Enter your new UPN user:"
read nuser

ansible -i $envloc/dev/hosts all -a "usermod -l $nuser $ouser" --become
ansible -i $envloc/qa/hosts all -a "usermod -l $nuser $ouser" --become 
ansible -i $envloc/pro2/hosts all -a "usermod -l $nuser $ouser" --become 
ansible -i $envloc/pro3/hosts all -a "usermod -l $nuser $ouser" --become 
ansible -i $envloc/pro4/hosts all -a "usermod -l $nuser $ouser" --become 
ansible -i $envloc/bi/hosts all -a "usermod -l $nuser $ouser" --become 
ansible -i $envloc/corp/hosts all -a "usermod -l $nuser $ouser" --become 
