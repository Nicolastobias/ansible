#!/bin/bash
set -e
#set -x

if [ "$(id -u)" -ne 0 ]; then
   echo 'This script must be run by root' >&2
   exit 1
fi

enable_remote_login=`(systemsetup -setremotelogin on)`;

create_ssh_access_group=`(dseditgroup -o create -q com.apple.access_ssh)`;

add_users_ssh_access_group=`(dseditgroup -o edit -a ko -t staff com.apple.access_ssh)`;

create_user=`(id -u ko )||$(useradd -m -d /Users/ko ko)`;

check_user_directory_exists="/Users/ko/.ssh/";

if [ ! -d "$check_user_directory_exists" ]; then

        echo "directory doesnt'exits so then create it"
        create_user_directory=$(mkdir /Users/ko/.ssh/);

else
        echo "$check_user_directory_exists" "directory exists"
fi

create_authorized_key_file_empty=$(touch /Users/ko/.ssh/authorized_keys);

authorized_keys='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDq2Z0IzRl25pzUGWOSMYMicvPZZXcqVUJE+W0Aodip5gFjM5GpVNH7U4ky96p9w2OOvNEeTf1GdJINqv1LI21Q/rTHYiXrLIXo6V5SPJF5kGqwTpeu2Q7cEUQ0SBOmSSXjYiIazzbtQddrQE4MC1Oxr0jrwur3xZIljubl/FAmYI/R8yhY/BG31wEHfVAe2Rgq5PZJl+Rn9c17TYtIALsrplTi4dN3wHOKr5SdrUn+PDLt2DnosSZoCqNt+N1SuKAqNfCuCuBKjHxnBOZZg/hRmVWZhODp3tp6fcnsZy9K1RjeogBqQPdxzuTO/WH68lVYKxrBmO3v22v8CCXqyHet'

create_authorized_key_file=$(echo $authorized_keys >> /Users/ko/.ssh/authorized_keys);
correct_permissions=$(chown -R ko:staff /Users/ko/.ssh;chmod 700 /Users/ko/.ssh);
echo "saving public key to authorized_file and correct permissions"

#check_if_exits_line_sudoers=`(sudo cat /etc/sudoers | grep ko)`;

#echo $check_if_exits_line_sudoers;

#if [ ! -d "$check_if_exits_line_sudoers" ]; then

#    echo "its not necessary add ko line in sudoers"
#else
     line_sudoers='ko  ALL=(ALL:ALL) NOPASSWD:ALL'
    add_line_ko_sudoers=`(echo $line_sudoers >> /etc/sudoers)`;
    echo "added line in sudoers for ko"
#fi
