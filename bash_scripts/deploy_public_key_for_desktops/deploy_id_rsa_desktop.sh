#!/bin/bash
set -e
#set -x

if [ "$(id -u)" -ne 0 ]; then
   echo 'This script must be run by root' >&2
   exit 1
fi


create_user=`(id -u ko )||$(useradd -m -d /home/ko ko)`;


check_user_directory_exists="/home/ko/.ssh/";

if [ ! -d "$check_user_directory_exists" ]; then

        echo "directory doesnt'exits so then create it"
        create_user_directory=$(mkdir /home/ko/.ssh/);

else
        echo "$check_user_directory_exists" "directory exists"
fi

create_authorized_key_file_empty=$(touch /home/ko/.ssh/authorized_keys);

authorized_keys='
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDq2Z0IzRl25pzUGWOSMYMicvPZZXcqVUJE+W0Aodip5gFjM5GpVNH7U4ky96p9w2OOvNEeTf1GdJINqv1LI21Q/rTHYiXrLIXo6V5SPJF5kGqwTpeu2Q7cEUQ0SBOmSSXjYiIazzbtQddrQE4MC1Oxr0jrwur3xZIljubl/FAmYI/R8yhY/BG31wEHfVAe2Rgq5PZJl+Rn9c17TYtIALsrplTi4dN3wHOKr5SdrUn+PDLt2DnosSZoCqNt+N1SuKAqNfCuCuBKjHxnBOZZg/hRmVWZhODp3tp6fcnsZy9K1RjeogBqQPdxzuTO/WH68lVYKxrBmO3v22v8CCXqyHet'

create_authorized_key_file=$(echo $authorized_keys >> /home/ko/.ssh/authorized_keys &> /dev/null);
correct_permissions=$(chown -R ko:ko /home/ko/.ssh;chmod 700 -R /home/ko/.ssh);
echo "saving public key to authorized_file and correct permissions"

install_packages=$(apt-get install openssh-server -y);

restart_sshd_service=$(sudo service ssh restart)

enable_sshd_service_at_boot=$(sudo systemctl enable ssh)

enable_sshd_service_at_boot=$(usermod -a -G adm ko)

enable_sshd_service_at_boot=$(usermod -a -G sudo ko)
