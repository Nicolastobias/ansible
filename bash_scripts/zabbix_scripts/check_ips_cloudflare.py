#! /usr/bin/env python

# ------------------------------------------------------------------------------------------
#      Checking Cloudflare's IPs
# - Date: May 01, 2018
# - Author: Luis Rodriguez
# - Description:
#    The script gets from cloudflare URL their IP ranges and compare them with the info in the "current.txt" file 
#    which is the same that we have in the fortigate devices. If the ranges are different the current.txt file is 
#    updated with the new info.
# - Return codes:
#     0: the IP ranges are the same
#     1: the ip ranges are diferent
# --------------------------------------------------------------------------------------------

import difflib
import requests
from subprocess import call
from sys import exit
from StringIO import StringIO

response = requests.get('https://www.cloudflare.com/ips-v4')
current_value = response.content
#print current_value

f=open("/usr/lib/zabbix/externalscripts/current.txt", "r")
used_value = f.read()
f.close()

if used_value == current_value:
      print "0"
else:
      print "1"
