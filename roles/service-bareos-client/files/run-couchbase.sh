#!/bin/sh
#removal of files older than 7 days
find /var/couchbase-bak/* -type d -ctime +1 -exec re -Rvf {} \;

#Creacion of node backup
/opt/couchbase/bin/cbbackup http://localhost:8091 /var/couchbase-bak/ -u Administrator -p password --single-node
