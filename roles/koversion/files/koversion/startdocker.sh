#!/bin/bash

docker rm -f koversion
docker build -t ko/koversion .
pkill -f koversion.py
docker run -p 8000:80 --restart always --name koversion -v /var/run/docker.sock:/var/run/docker.sock -e env_host=$HOSTNAME -d ko/koversion
