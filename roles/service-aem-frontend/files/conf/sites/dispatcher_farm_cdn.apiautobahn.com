 /cdnapiautobahn
    {
	
    # Request headers that should be forwarded to the remote server.
    #/clientheaders
     # {
     # "*"
     # }
     $include "global/dispatcher_clientheaders"

    # Hostname globbing for farm selection (virtual domain addressing)
    /virtualhosts
      {
	     #MUST be changed for each website
		 cdn.apiautobahn.com
		 #$include "dispatcher_vhost_cdn.apiautobahn.com"
      }

    # The load will be balanced among these render instances
    /renders
      {
      /rend01
        {
		#the publish instances for the azure servers
		  $include "global/dispatcher_renders"
        }
      }

    /filter
      {
	  #sets the filters - generic for all sites
	  $include "global/dispatcher_filters"
      }

    # The cache section regulates what responses will be cached and where.
    /cache
      {
	  #below includes provides the docroot for the site - MUST BE CHANGED changes per site
	  #$include "dispatcher_docroot_api.apiautobahn.com"
	  /docroot "/var/www/html/content/dam/cdn.apiautobahn.com"
	  
      # Sets the level upto which files named ".stat" will be created in the
      # document root of the webserver. When an activation request for some
      # page is received, only files within the same subtree are affected
      # by the invalidation.
      /statfileslevel "6"
	  
      # Flag indicating whether to cache responses to requests that contain
      # authorization information.
      /allowAuthorized "1"
	  
      # Flag indicating whether the dispatcher should serve stale content if
      # no remote server is available.
      /serveStaleOnError "0"
	  
      /rules
        {
         $include "global/dispatcher_rules"
        }

      /invalidate
        {
		$include "global/dispatcher_invalidate"
        }

      /allowedClients
        {
		$include "global/dispatcher_allowedclients"
        }
      /ignoreUrlParams
         {
	      $include "global/dispatcher_ignoreurlparams"
         }
       /enableTTL "1"
      }
    /statistics
      {
	  $include "global/dispatcher_statistics"
      }
    }
