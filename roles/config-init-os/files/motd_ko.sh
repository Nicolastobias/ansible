#!/bin/bash
# Server Status Script
# Version 0.1.3
# Updated: July 26th 2011
#Interfaces
INTERFACE=$(ip -4 ad | grep 'state UP' | awk -F ":" '!/^[0-9]*: ?lo/ {print $2}')
CPUTIME=$(ps -eo pcpu | awk 'NR>1' | awk '{tot=tot+$1} END {print tot}')
CPUCORES=$(cat /proc/cpuinfo | grep -c processor)
UP=$(echo `uptime` | awk '{ print $3 " " $4 }')
echo "
– Server Name               = `hostname`
– OS Version                = `cat /etc/redhat-release`
– Load Averages             = `cat /proc/loadavg`
– System Uptime             = `echo $UP`
– Platform Data             = `uname -orpi`
– Memory free (real)        = `free -m | head -n 2 | tail -n 1 | awk {'print $4'}` Mb
– Memory free (cache)       = `free -m | head -n 3 | tail -n 1 | awk {'print $3'}` Mb
– Swap    use               = `free -m | tail -n 1 | awk {'print $3'}` Mb
– Disk Space Used           = `df / | awk '{ a = $4 } END { print a }'`

*******************************************************************************

`figlet interfaces`

*******************************************************************************
"
for x in $INTERFACE
do
        MAC=$(ip ad show dev $x |grep link/ether |awk '{print $2}')
        IP=$(ip ad show dev $x |grep -v inet6 | grep inet|awk '{print $2}')
        printf "NAME: "$x"\t""MAC: "$MAC"\t""IP: "$IP"\t\n"
done
echo "
*******************************************************************************

 "
