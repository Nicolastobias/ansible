#!/bin/bash

mkdir -p /etc/haproxy/ipranges/

cd /etc/haproxy/ipranges/

wget -N http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip
unzip GeoIPCountryCSV.zip

cut -d, -f1,2,5 GeoIPCountryWhois.csv | iprange | sed 's/\"//g' > GeoIPCountryWhois.txt
patch -p0 < /tmp/geoip_pe.patch

rm -rf /etc/haproxy/ipranges/GeoIPCountryWhois.zip
rm -rf /etc/haproxy/ipranges/GeoIPCountryWhois.csv
