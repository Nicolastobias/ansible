
#Stop Ambari services
#$1 = username
#$2 = password
#$3 = service
#$4 = cluster name
#$5 = ambari-service
#$6 = INSTALLED/STARTED
#$7 = hadoop hostname
/usr/bin/curl --user $1:$2 -i -X PUT -d "{\"RequestInfo\": {\"context\": \"start $5\"}, \"ServiceInfo\": {\"state\": \"$6\"}}" http://$7:8080/api/v1/clusters/$4/services/$5
