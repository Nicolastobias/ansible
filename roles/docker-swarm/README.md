Ansible Role: Docker Swarm
==========================

Setup a Docker Swarm cluster on RHEL/CentOS using the new Docker Engine's "Swarm Mode" (https://docs.docker.com/engine/swarm/).

Requirements
------------

None :-)

Role Variables
--------------

    docker_swarm_interface: "{{ ansible_default_ipv4['alias'] }}"

Setting `docker_swarm_interface` allows you to define which network interface will be used for cluster inter-communication.

    docker_swarm_addr: "{{ hostvars[inventory_hostname]['ansible_' + docker_swarm_interface]['ipv4']['address'] }}"

By default, the ip address for raft API will be taken from desired interface.
You can setup listening address where the raft APIs will be exposed, overriding
the `docker_swarm_addr` variable value in your playbook.

    docker_swarm_port: 2377

Listening port where the raft APIs will be exposed.

    docker_admin_users:
      - "{{ ansible_user }}"

The list of users that has to be added to the `docker_group` to interact with the Docker daemon.
**NB**: The users must already exist in the system.

    skip_engine: False
    skip_group: False
    skip_swarm: False
    skip_docker_py: False

Setting `skip_engine: True` will make the role skip the installation of `docker-engine`.
If you want to use this role to just install `docker-engine` without enabling `swarm-mode` set `skip_swarm: True`.
To skip the tasks adding the `docker_admin_users` to the `docker_group` set `skip_group: True`.
Finally, the `docker-py` installation task can be skipped setting `skip_docker_py` to `True`.

Swarm node labels
-----------------

[Node labels](https://docs.docker.com/engine/swarm/manage-nodes/#add-or-remove-label-metadata) provide a
flexible method of node organization. You can also use node labels in service constraints.
Apply constraints when you create a service to limit the nodes where the scheduler assigns tasks for the service.
You can define labels by `swarm_labels` variable, e.g:

    $ cat inventory
    ...
    [docker_swarm_manager]
    swarm-01 swarm_labels=deploy

    [docker_swarm_worker]
    swarm-02 swarm_labels='["libvirt", "docker", "foo", "bar"]'
    swarm-03
    ...

In this case:

    $ docker inspect --format '{{json .Spec.Labels}}'  swarm-02 | jq
    {
       "bar": "true",
       "docker": "true",
       "for": "true",
       "libvirt": "true",
    }

You can assign labels to cluster running playbook with `--tags=swarm_labels`

**NB**: Please note, all labels that are not defined in inventory will be removed

Dependencies
------------

None :-)

